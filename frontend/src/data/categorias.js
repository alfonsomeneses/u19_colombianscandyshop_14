import axios from "axios";
import { BASE_URL } from "../config/constants";

export const categorias = [
    {codigo:"CHOCO",nombre:"Chocolateria"},
    {codigo:"CONF",nombre:"Confitería"},
    {codigo:"REPO",nombre:"Repostería"},
    {codigo:"AREQ",nombre:"Arequipe"},
    {codigo:"TIPICO",nombre:"Dulces Típicos"}
]