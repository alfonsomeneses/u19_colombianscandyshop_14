import React, { useEffect, useState } from 'react'
import { Outlet } from 'react-router-dom'
import Navbar from '../shared/navbar'
import Sidebar from '../shared/sidebar'
import { useNavigate, Link } from "react-router-dom";

export default function Admin() {
  const navigate = useNavigate();
  const [reload,setReload] = useState(false)

  useEffect(() => {
    
    const token =  localStorage.getItem("token");
    console.log(token);
    if (!token) {
      navigate("/login")
    }

    const carrito = localStorage.getItem("carrito_compra");
    if (!carrito) {
      localStorage.setItem("carrito_compra",'{"productos":[]}')
    }
    
    

    return () => {}
  }, [reload])

  return (
    <div className="wrapper">
    <Navbar/>
    <Sidebar/>
    <div className="content-wrapper">
      <Outlet />
    </div>
  </div>
  )
}
