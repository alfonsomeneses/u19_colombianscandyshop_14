import axios from "axios"
import React, { useEffect, useState } from "react"
import Header from "../../shared/header"
import { useForm } from "react-hook-form"
import { ALERT, BASE_URL, getPayload } from "../../config/constants"
import { useParams } from "react-router-dom"

export default function UpdateProducto() {
  const params = useParams()
  const payload = getPayload()
  const headers = { Authorization: localStorage.getItem("token") }

  const [producto, setProducto] = useState({
    nombre: "",
    cantidad: 0,
    precio: 0,
    imagen: "",
    vendedor: "",
  })

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm()

  useEffect(() => {
    // GET PRODUCTO A MODIFICAR
    if (params.id !== "new") {
      axios
        .get(BASE_URL + "/producto", {
          headers,
          params: { id: params.id },
        })
        .then((res) => {
          setProducto(res.data)
          reset(res.data)
        })
        .catch((err) => {
          ALERT.fire({ icon: "error", title: "Hubo un error inesperado" })
        })
    }
    return () => {}
  }, [])

  function submit(data) {
    // ASOCIAMOS EL PRODUCTO CON EL VENDEDOR
    data.vendedor = payload.id
    //CONDICIONAMOS LA PETICION
    if (params.id === "new") {
      // PETICION PARA CREAR USUARIO
      axios
        .post(BASE_URL + "/producto", data, { headers })
        .then((res) => {
          ALERT.fire({
            icon: "success",
            title: "Se creo el producto " + data.nombre,
          })
        })
        .catch((err) => {
          console.error(err)
          ALERT.fire({
            icon: "error",
            title: "Hubo un error al crear el producto",
          })
        })
    } else {
      // PETICION PARA MODIFICAR PRODUCTO
      data.id = producto._id
      axios
        .put(BASE_URL + "/producto", data, { headers })
        .then((res) => {
          ALERT.fire({
            icon: "success",
            title: "Se modificó el producto " + data.nombre,
          })
        })
        .catch((err) => {
          console.error(err)
          ALERT.fire({
            icon: "error",
            title: "Hubo un error al crear el producto",
          })
        })
    }
  }

  return (
    <>
      <Header title="Actualizar producto" pathName="Producto" path="/" />
      <section className="content">
        <div className="container-fluid">
          <div className="row">
            <div className="col">
              <div className="card card-primary">
                <div className="card-header">
                  <h3 className="card-title">producto</h3>
                </div>
                <form onSubmit={handleSubmit(submit)}>
                  <div className="card-body">
                    <div className="form-group">
                      <label htmlFor="nombre">Nombre del producto</label>
                      <input
                        type="text"
                        className={
                          "form-control" + (errors.nombre ? " is-invalid" : "")
                        }
                        id="nombre"
                        {...register("nombre", { required: true })}
                        placeholder="Ejemplo: Obleas x12"
                        defaultValue={
                          params.id !== "new" ? producto?.nombre : ""
                        }
                      />
                      {errors.nombre && (
                        <span className="text-danger">
                          El nombre del producto es obligatorio
                        </span>
                      )}
                    </div>

                    <div className="form-group">
                      <label htmlFor="cantidad">Cantidad disponible</label>
                      <input
                        type="number"
                        placeholder="Agrega la cantidad"
                        id="cantidad"
                        className={
                          "form-control" +
                          (errors.cantidad ? " is-invalid" : "")
                        }
                        defaultValue={
                          params.id !== "new" ? producto?.cantidad : ""
                        }
                        {...register("cantidad", { required: true, min: 0 })}
                      />
                      {errors.cantidad && (
                        <span className="text-danger">
                          El campo cantidad es obligatorio
                        </span>
                      )}
                    </div>

                    <div className="form-group">
                      <label htmlFor="precio">Precio del producto</label>
                      <input
                        type="number"
                        id="precio"
                        className={
                          "form-control" + (errors.precio ? " is-invalid" : "")
                        }
                        defaultValue={
                          params.id !== "new" ? producto?.precio : ""
                        }
                        {...register("precio", { required: true, min: 0 })}
                        placeholder="Ingresa el precio"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="imagen">Carga la imagen</label>
                      <div className="input-group">
                        <div className="custom-file">
                          <input
                            type="file"
                            className="custom-file-input"
                            id="imagen"
                            {...register("imagen", { required: true})}
                          />
                          <label
                            className="custom-file-label"
                            htmlFor="imagen">
                            Selecciona la imagen
                          </label>
                        </div>
                        <div className="input-group-append">
                          <span className="input-group-text">Cargar</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card-footer">
                    <button type="submit" className="btn btn-primary">
                      Modificar o Crear producto
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}
