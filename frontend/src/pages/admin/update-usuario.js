import axios from "axios"
import React, { useEffect, useState } from "react"
import Header from "../../shared/header"
import { useForm } from "react-hook-form"
import { ALERT, BASE_URL } from "../../config/constants"
import { useParams } from "react-router-dom"

export default function UpdateUsuario() {
  const params = useParams()
  const headers = { Authorization: localStorage.getItem("token") }

  const [usuario, setUsuario] = useState(null)

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset
  } = useForm()

  //
  useEffect(() => {
    if (params.id !== "new") {
      axios
        .get(BASE_URL + "/cliente", {
          headers,
          params: { id: params.id },
        })
        .then((res) => {
          setUsuario(res.data)
          reset(res.data)
        })
        .catch((err) => {
          ALERT.fire({ icon: "error", title: "Hubo un error inesperado" })
        })
    }
    return () => {}
  }, [])

  function submit(data) {
    //CONDICIONAMOS LA PETICION
    if (params.id === "new") {
      // PETICION PARA CREAR USUARIO
      axios
        .post(BASE_URL + "/cliente", data, {headers})
        .then((res) => {
          ALERT.fire({
            icon: "success",
            title: "Se creo el usuario " + data.nombre,
          })
        })
        .catch((err) => {
          console.error(err)
          ALERT.fire({
            icon: "error",
            title: "Hubo un error al crear el usuario",
          })
        })
    } else {
      // PETICION PARA MODIFICAR USUARIO
      data.id = usuario._id
      axios
        .put(BASE_URL + "/cliente", data, {headers})
        .then((res) => {
          ALERT.fire({
            icon: "success",
            title: "Se modificó el usuario " + data.nombre,
          })
        })
        .catch((err) => {
          console.error(err)
          ALERT.fire({
            icon: "error",
            title: "Hubo un error al crear el usuario",
          })
        })
    }
  }

  return (
    <>
      <Header title="Actualizar Usuario" pathName="Usuario" path="/" />
      <section className="content">
        <div className="container-fluid">
          <div className="row">
            <div className="col">
              <div className="card card-primary">
                <div className="card-header">
                  <h3 className="card-title">Usuario</h3>
                </div>
                <form onSubmit={handleSubmit(submit)}>
                  <div className="card-body">
                    <div className="form-group">
                      <label htmlFor="nombre">Nombres</label>
                      <input
                        type="text"
                        className={
                          "form-control" + (errors.nombre ? " is-invalid" : "")
                        }
                        id="nombre"
                        {...register("nombre", { required: true })}
                        placeholder="Ejemplo: Jhon"
                        defaultValue={
                          params.id !== "new" ? usuario?.nombre : ""
                        }
                      />
                      {errors.nombre && (
                        <span className="text-danger">
                          El campo nombres es obligatorio
                        </span>
                      )}
                    </div>

                    <div className="form-group">
                      <label htmlFor="apellido">Apellidos</label>
                      <input
                        type="text"
                        placeholder="Ejemplo: Doe"
                        id="apellido"
                        className={
                          "form-control" +
                          (errors.apellido ? " is-invalid" : "")
                        }
                        defaultValue={
                          params.id !== "new" ? usuario?.apellido : ""
                        }
                        {...register("apellido", { required: true })}
                      />
                      {errors.apellido && (
                        <span className="text-danger">
                          El campo apellidos es obligatorio
                        </span>
                      )}
                    </div>

                    <div className="form-group">
                      <label>Tipo de documento</label>
                      <select
                        defaultValue={
                          params.id !== "new" ? usuario?.tipoDocumento : ""
                        }
                        className={
                          "form-control" +
                          (errors.tipoDocumento ? " is-invalid" : "")
                        }
                        {...register("tipoDocumento", { required: true })}>
                        <option value={"CC"}>Cédula de ciudadanía</option>
                        <option value={"CE"}>Cédula de extranjeria</option>
                        <option value={"RE"}>Registro Civil</option>
                        <option value={"PA"}>Pasaporte</option>
                      </select>
                    </div>

                    <div className="form-group">
                      <label htmlFor="numDocumento">Número de documento</label>
                      <input
                        type="text"
                        id="numDocumento"
                        className={
                          "form-control" +
                          (errors.numDocumento ? " is-invalid" : "")
                        }
                        defaultValue={
                          params.id !== "new" ? usuario?.numDocumento : ""
                        }
                        {...register("numDocumento", { required: true })}
                        placeholder="Ingresa el numero de documento"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="telefonoContacto">
                        Telefono de contacto
                      </label>
                      <input
                        type="number"
                        id="telefonoContacto"
                        className={
                          "form-control" +
                          (errors.telefonoContacto ? " is-invalid" : "")
                        }
                        defaultValue={
                          params.id !== "new" ? usuario?.telefonoContacto : ""
                        }
                        {...register("telefonoContacto", { required: true })}
                        placeholder="Ejemplo: 123456789"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="email">Correo electronico</label>
                      <input
                        type="email"
                        id="email"
                        className={
                          "form-control" + (errors.email ? " is-invalid" : "")
                        }
                        defaultValue={params.id !== "new" ? usuario?.email : ""}
                        {...register("email", { required: true })}
                        placeholder="Ingresa el email"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="email">Contraseña</label>
                      <input
                        type="password"
                        className={
                          "form-control" +
                          (errors.password ? " is-invalid" : "")
                        }
                        id="password"
                        {...register("password", { required: true })}
                        placeholder="Contraseña"
                      />
                    </div>
                  </div>
                  <div className="card-footer">
                    <button type="submit" className="btn btn-primary">
                      Modificar o Crear usuario
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}
