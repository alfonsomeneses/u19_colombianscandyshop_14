import axios from "axios";
import React from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import { ALERT, BASE_URL } from "../config/constants";

export default function ForgotPassword() {
  const navigate = useNavigate();

  async function formulario(event) {
    
    event.preventDefault(); // Evita que HTML reenvie una petición

    const { email } = event.target;

    try {
        const res = await axios.post(BASE_URL + "/usuario/password/recovery", {
          email: email.value,
        });
        localStorage.setItem("token", res.data.token);
        navigate("/login");
      } catch (error) {
        alert("Error interno, intente mas tarde");
      }
  }

  return (
    <div className="login-page">
      <div className="login-box">
        <div className="card">
          <div className="card-body login-card-body">
            <p className="login-box-msg">Recuperación Contraseña</p>
            <form onSubmit={formulario}>
              <div className="input-group mb-3">
                <input
                  type="email"
                  className="form-control"
                  name="email"
                  placeholder="Correo electrónico"
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope" />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <button type="submit" className="btn btn-info btn-block">
                    Recuperar contraseña
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
