import React from "react"

import "./mantenimiento.css"

export default function Mantenimiento() {
  return (
    <div className="maintenance">
      <div className="maintenance_contain">
        <img
          src="https://demo.wpbeaveraddons.com/wp-content/uploads/2018/02/main-vector.png"
          alt="maintenance"
        />
        <span className="pp-infobox-title-prefix">¡PROXIMAMENTE DULCESAPP!</span>
        <div className="pp-infobox-title-wrapper">
          <h3 className="pp-infobox-title">La pagina esta en mantenimiento</h3>
        </div>
      </div>
    </div>
  )
}
