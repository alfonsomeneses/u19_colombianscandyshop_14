import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";

export default function Navbar() {
  
  function revisarProductos(event) {
    alert("Ver el detalle del carrito de compra en la proxima versión!")
  }

  return (
    <nav className="main-header navbar navbar-expand navbar-white navbar-light">
      <ul className="navbar-nav">
        <a className="nav-link" data-widget="pushmenu" href="#" role="button">
          <i className="fas fa-bars"></i>
        </a>
        <li className="nav-item">
          <a className="nav-link">Inicio</a>
        </li>
        <li className="nav-item">
          <a className="nav-link">Producto</a>
        </li>

        <div className="btn-group" role="group" aria-label="Opciones">
        <i className="fa-sharp fa-solid fa-cart-shopping"></i>
          <button className="btn btn-info" onClick={revisarProductos}>
            Articulos: 0
          </button>
        </div>
      </ul>
    </nav>
  );
}
