const mongoose = require("mongoose")
const dotenv = require("dotenv").config()

const conexionDB = () => {
  mongoose
    .connect(process.env.DB_CONNECTION)
    .then(() => {
      console.log("DB Connection Successful");
    })
    .catch((error) => {
      console.log("Error trying to connect to de DB:");
      console.log(error);
    })
}

module.exports = conexionDB
