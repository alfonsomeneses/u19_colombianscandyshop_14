const { request, response } = require("express");
const { Roles, Categorias, MetodosPagos } = require("../config/config").DB;

const UsuarioModel = require("../models/usuario");
const RoleModel = require("../models/rol");
const CategoriaModel = require("../models/categoria")
const MetodosPagosModel = require("../models/metodo_pago")

async function configRoles(req = request, res = response) {
  const cantidadRoles = await RoleModel.count();
  if (cantidadRoles > 0) {
    res.send({ mensaje: "Ya se ha hecho la configuración de los roles" });
  } else {
    RoleModel.create(Roles)
      .then((rolesCreados) => {
        res
          .status(201)
          .send({ mensaje: "Creación de Roles Exitosamente", rolesCreados });
      })
      .catch(() => {
        res.status(400).send({ mensaje: "No se logro crear los roles" });
      });
  }
}

async function configCategorias(req = request, res = response) {
    const cantidadCategorias = await CategoriaModel.count();
  if (cantidadCategorias > 0) {
    res.send({ mensaje: "Ya se ha hecho la configuración de las categorias" });
  } else {
    CategoriaModel.create(Categorias)
      .then((categoriasCreadas) => {
        res
          .status(201)
          .send({ mensaje: "Creación de las Categorias Exitosamente", categoriasCreadas });
      })
      .catch(() => {
        res.status(400).send({ mensaje: "No se logro crear las categorias" });
      });
  }
}

async function configMetodosDePago(req = request, res = response) {
    const cantidadMetodos = await MetodosPagosModel.count();
  if (cantidadMetodos > 0) {
    res.send({ mensaje: "Ya se ha hecho la configuración de los metodos de pago" });
  } else {
    MetodosPagosModel.create(MetodosPagos)
      .then((metodosCreadas) => {
        res
          .status(201)
          .send({ mensaje: "Creación de los Metodos De Pago Exitosamente", metodosCreadas });
      })
      .catch(() => {
        res.status(400).send({ mensaje: "No se logro crear los metodos de pago" });
      });
  }
}

module.exports = { configRoles, configCategorias, configMetodosDePago };
