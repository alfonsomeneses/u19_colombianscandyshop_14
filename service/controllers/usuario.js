const { request, response } = require("express");
const { hashSync, genSaltSync, compareSync } = require("bcryptjs");
const nodemailer = require('nodemailer');
const UsuarioModel = require("../models/usuario");
const RoleModel = require("../models/rol");
const DireccionModel = require("../models/direccion");

//Crear Usuario
async function crearUsuario(req = request, res = response) {
  const { email, password, nombre, apellido } = req.body;

  if (!email || !password || !nombre || !apellido) {
    res.status(400).send({ mensaje: "Faltan campos requeridos" });
    return;
  }

  const usuarioEncontrado = await UsuarioModel.findOne({
    email,
  });

  if (usuarioEncontrado) {
    res
      .status(400)
      .send({ mensaje: "Ya existe un usuario con el email: " + email });
  } else {
    const passwordEncrypted = hashSync(password, genSaltSync());
    req.body.password = passwordEncrypted;
    
    req.body.id = Date.now();

    const rolUsuario = await RoleModel.findOne({ codigo: "USER" });

    req.body.rol = rolUsuario;

    UsuarioModel.create(req.body)
      .then((usuarioCreado) => {
        const datosUsuario = {
          id: usuarioCreado.id,
          email: usuarioCreado.email,
          nombre: usuarioCreado.nombre,
          apellido: usuarioCreado.apellido,
          role: usuarioCreado.rol.nombre,
        };

        res
          .status(201)
          .send({ mensaje: "Se creo el usuario", usuario: datosUsuario });
      })
      .catch(() => {
        res.send({ mensaje: "No se logro crear el usuario" });
      });
  }
}

async function agregarDireccionEnvio(req = request, res = response) {
  try {
    if (!req.body.ciudad || !req.body.direccion || !req.body.usuario) {
      return res.status(400).send({ mensaje: "Faltan datos obligatorios" });
    }

    const usuario = await UsuarioModel.findOne({ id: req.body.usuario });

    if (!usuario) {
      return res
        .status(400)
        .send({ mensaje: "No existe un usuario con ese ID" });
    }

    req.body.usuario = usuario;

    DireccionModel.create(req.body)
      .then((dirCreada) => {
        return res.send({
          mensaje: "Se agrego la dirección",
          direccion: dirCreada,
        });
      })
      .catch((error) => {
        return res
          .status(400)
          .send({
            mensaje: "Error intentando agregar la dirección",
            error: error,
          });
      });
  } catch (error) {
    return res.status(400).send({ error: error });
  }
}

async function recuperarPassword(req = request, res = response){
  const {email} = req.body;

  const usuario = await UsuarioModel.findOne({email:email});

  if (!usuario) {
      return res.status(400).send({mensaje:"No existe un usuario con este email"});
  }
  const newPassword = generarPassword();

  const passwordEncrypted = hashSync(newPassword, genSaltSync());
  
  usuario.password = passwordEncrypted;
  
  var envioMail = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'colombianscandyshop@gmail.com',
        pass: 'rtvranyfezhtxabw'
      }
    });

  const mensajeEmail = "Hola "+usuario.nombre+", tu nueva contraseña es: "+newPassword;

  var mailOptions = {
    from: 'colombianscandyshop@gmail.com',
    to: usuario.email,
    subject: 'Recuperando contraseña',
    text: mensajeEmail
  };

  envioMail.sendMail(mailOptions, function(error, info){
    if (error) {
      return res.status(400).send({mensaje:"Error interno",error})
    } else {
      usuario.save();
      console.log('Email enviado: ' + info.response);
      return res.send({mensaje:'Email enviado: ' + info.response});
      
    }
  });
}

function generarPassword(){
  const passwordSize = 4;
  let password = "";

  for (let i = 0; i < passwordSize; i++) {
      password += Math.floor(Math.random() * 9) + 1;
  }

  return password;
}

module.exports = { crearUsuario, agregarDireccionEnvio,recuperarPassword };
