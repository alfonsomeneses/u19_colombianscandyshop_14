const { request, response } = require("express");
const { verify } = require("jsonwebtoken");
const {Auth} = require("../config/config")

function validarToken(req = request, res = response, next) {
  const token = req.header("Authorization")

  try {
    if (token) {
      if (verify(token,Auth.Token)) {
        next()
      } else res.status(401).send({mensaje: "token invalido"})
    }
    else res.status(401).send({mensaje: "no existe el token"})    
  } catch (error) {
    res.status(500).send({error, mensaje: "Error tratando de validar la autenticación"})
  }
}

module.exports = validarToken