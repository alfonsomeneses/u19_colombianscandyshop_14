const { Schema, model } = require("mongoose")

const categoriaSchema = new Schema({
  codigo: String,
  nombre: String,
})

const CategoriaModel = model("categorias", categoriaSchema)

module.exports = CategoriaModel