const { Schema, model } = require("mongoose")

const compraSchema = new Schema({
  fecha: Date,
  codigo_factura: String,
  direccion: String,
  valor_total: Number,
  usuario: {
    type: Schema.Types.ObjectId,
    ref: "usuarios",
  },
  metodoPago: {
    type: Schema.Types.ObjectId,
    ref: "metodo_pagos"
  }
})

const CompraModel = model("compras", compraSchema)

module.exports = CompraModel