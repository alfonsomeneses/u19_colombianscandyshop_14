const { Schema, model } = require("mongoose");

const direccionSchema = new Schema({
  ciudad: String,
  direccion: String,
  descripcion: String,
  usuario: {
    type: Schema.Types.ObjectId,
    ref: "usuarios",
  },
});

const DireccionModel = model("direcciones", direccionSchema);

module.exports = DireccionModel;
