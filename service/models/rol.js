const { Schema, model } = require("mongoose")

const rolSchema = new Schema({
  codigo: String,
  nombre: String,
})

const RolModel = model("roles", rolSchema)

module.exports = RolModel