const { Schema, model } = require("mongoose");

const usuarioSchema = new Schema({
  id: Number,
  email: String,
  nombre: String,
  apellido: String,
  password: String,
  rol: {
    type: Schema.Types.ObjectId,
    ref: "roles",
  },
});

const UsuarioModel = model("usuarios", usuarioSchema);

module.exports = UsuarioModel;
