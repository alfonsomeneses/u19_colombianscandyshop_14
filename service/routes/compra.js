const { Router } = require("express");
const validarToken = require("../middlewares/auth");
const {
  generarCompra,
  obtenerCompras,
  obtenerDetallesCompra,
} = require("../controllers/compra");

const routerCompra = Router();

routerCompra.post("/:id", [validarToken], generarCompra);
routerCompra.get("/:id", [validarToken],obtenerCompras);
routerCompra.get("/detalle/:codigo",[validarToken], obtenerDetallesCompra);

module.exports = routerCompra;
