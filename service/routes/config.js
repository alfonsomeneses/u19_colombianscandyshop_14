const { Router } = require("express")
const {configRoles, configCategorias, configMetodosDePago} = require("../controllers/config")
const validarToken = require("../middlewares/auth")
const routerConfig = Router()

routerConfig.post("/roles",[validarToken],configRoles)
routerConfig.post("/categorias",[validarToken],configCategorias)
routerConfig.post("/metodos_pagos",[validarToken],configMetodosDePago)

module.exports = routerConfig;