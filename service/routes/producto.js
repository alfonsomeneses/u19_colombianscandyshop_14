const { Router } = require("express");
const validarToken = require("../middlewares/auth");
const {
  obtenerCategorias,
  obtenerProductos,
  crearProducto,
  guardarImagen,
  obtenerImagen
} = require("../controllers/producto");

const routerConfig = Router();

routerConfig.get("", obtenerProductos);
routerConfig.post("", [validarToken],crearProducto);
routerConfig.get("/categoria", obtenerCategorias);
routerConfig.put("/imagen",[validarToken], guardarImagen);
routerConfig.get("/imagen", obtenerImagen)

module.exports = routerConfig;
